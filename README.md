# IOC for TS2 MKS vacuum gauge controllers and gauges

## Used modules

*   [vac_ctrl_mks946_937b](https://gitlab.esss.lu.se/e3/wrappers/vac/e3-vac_ctrl_mks946_937b)


## Controlled devices

*   TS2-020Row:Vac-VEG-01100
    *   TS2-010CRM:Vac-VGC-01100
    *   TS2-010CRM:Vac-VGP-01100
