#
# Module: essioc
#
require essioc

#
# Module: vac_ctrl_mks946_937b
#
require vac_ctrl_mks946_937b,4.5.2


#
# Setting STREAM_PROTOCOL_PATH
#
epicsEnvSet(STREAM_PROTOCOL_PATH, "${vac_ctrl_mks946_937b_DB}")


#
# Module: essioc
#
iocshLoad("${essioc_DIR}/common_config.iocsh")

#
# Device: TS2-020Row:Vac-VEG-01100
# Module: vac_ctrl_mks946_937b
#
iocshLoad(${vac_ctrl_mks946_937b_DIR}/vac_ctrl_mks946_937b_moxa.iocsh, "DEVICENAME = TS2-020Row:Vac-VEG-01100, IPADDR = moxa-vac-ts2.tn.esss.lu.se, PORT = 4001, BOARD_A_SERIAL_NUMBER = 1902131114, BOARD_B_SERIAL_NUMBER = 1812101207, BOARD_C_SERIAL_NUMBER = 1812100755")

#
# Device: TS2-010CRM:Vac-VGC-01100
# Module: vac_ctrl_mks946_937b
#
iocshLoad(${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgc.iocsh, "DEVICENAME = TS2-010CRM:Vac-VGC-01100, CHANNEL = B1, CONTROLLERNAME = TS2-020Row:Vac-VEG-01100")

#
# Device: TS2-010CRM:Vac-VGP-01100
# Module: vac_ctrl_mks946_937b
#
iocshLoad(${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgp.iocsh, "DEVICENAME = TS2-010CRM:Vac-VGP-01100, CHANNEL = A1, CONTROLLERNAME = TS2-020Row:Vac-VEG-01100")
